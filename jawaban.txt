1. Soal Pertama Membuat Database
create database myshop;

2. Soal Kedua Membuat Table di Dalam Database
-Membuat tabel users
 create table users(
    -> id int(255) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255));

-Membuat tabel categories
create table categories(
    -> id int(255) primary key auto_increment,
    -> name varchar(255));

-Membuat tabel items
create table items(
    -> id int(255) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(255),
    -> stock int(255),
    -> category_id int(255),
    -> foreign key(category_id) references categories(id));

3. Soal Ketiga Memasukkan Data pada Table
-Tabel users
 insert into users (name, email, password) values ("John Doe","john@doe.com","john123"), ("Jane Doe","jane@doe.com","jenita123");

-Tabel categories
insert into categories (name) values ("gadget"),("cloth"),("men"),("women"),("branded");

-Tabel items
insert into items (name,description,price,stock,category_id) values ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2), ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Soal Keempat Mengambil Data dari Database
a. Mengambil data users pada tabel users kecuali password nya
select id, name, email from users;

b. Mengambil data items
-Query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
select * from items where price > 1000000;

-Query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
select * from items where name like 'uniklo%';
select * from items where name like '%watch';

c. Menampilkan data items join dengan kategori 
query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items (gunakan join)
select items.name,items.description,items.price,items.stock,items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

5. Soal Kelima Mengubah Data dari Database
Ubah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000.
update items set price = 2500000 where id=1;